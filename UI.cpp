#include "UI.h"

UI::UI(std::ostream& outputStream, Game* gamePtr) : game(gamePtr), outputStream(outputStream) {}

void UI::draw() {
    if (end) {
        drawEnd();
        }
    else if (changed)
        redraw();
    outputStream <<  ss.str() << std::endl;
    changed = false;
}

void UI::redraw() {
    ss << CLEAR << RESET;
    ss << "\r";

    for (auto player : game->getPlayers()) {
        if (player->getBlackjack())
            ss << " *BLACKJACK* ";
        else if (player->isStanding() || player->handValue() >= 21)
            ss << " *STANDING* ";
        setConsoleColor(player->getId(), ss);
        ss << "Player" << player->getId() << ": " << player->getHand().toString() << RESET << "\n\r";
    }

    ss << "Dealer: " << game->getDealer()->getHand().toString() << "\n\r";

    setConsoleColor(game->getActivePlayer()->getId(), ss);
    if (game->getActivePlayer()->getId() == game->getDealer()->getId()) {
        ss << "Dealer's turn\n";
    } else {
        ss << "Player " << game->getActivePlayer()->getId() << " turn: ";
    }
    ss << RESET << "\n\r";
}

void UI::drawEnd() {
    ss << CLEAR << "\r";
    auto dealer = game->getDealer();
    for (auto player : game->getPlayers())
        ss << gameOutcome(player) << "Player" << player->getId() << ": "
           << "" << player->getHand().toString() << RESET << "\n\r";
    ss << "Dealer: " << dealer->getHand().toString() << "\n\r";
    ss << "\n" << RESET;
}

std::string UI::gameOutcome(Player* player) {
    auto dealer = game->getDealer();
    if (player->getBlackjack())
        return BLUE "*BLACKJACK* ";
    else if (player->handValue() > 21 || (player->handValue() < dealer->handValue() && dealer->handValue() <= 21))
        return RED "*LOSE* ";
    else if (dealer->handValue() > 21 || (player->handValue() > dealer->handValue() && player->handValue() <= 21))
        return GREEN "*WIN* ";
    else if (player->handValue() == dealer->handValue())
        return YELLOW "*DRAW* ";
    return "";
}

void UI::setConsoleColor(const size_t colorId, std::stringstream& buffer) {
    switch (colorId) {
        case 0:
            buffer << RED;
            break;
        case 1:
            buffer << BLUE;
            break;
        case 2:
            buffer << GREEN;
            break;
        case 3:
            buffer << YELLOW;
            break;
        case 4:
            buffer << CYAN;
            break;
        default:
            buffer << RESET;
            break;
    }
}

void UI::gameEnded() {
    end = true;
}

void UI::gameChanged() {
    changed = true;
}