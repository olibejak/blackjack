#include "Deck.h"
#include <algorithm>
#include <random>
#include <ctime>

Deck::Deck() : cardOnTopIndex(0) {
    for (auto& suit : {Suit::HEARTS, Suit::DIAMONDS, Suit::CLUBS, Suit::SPADES}) {
        for (auto& rank : { ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING }) {
            cardsInDeck.emplace_back(suit, rank);
        }
    }
}

std::vector<Card> Deck::getCards() const {
    return cardsInDeck;
}

Card  Deck::getCardOnTop() {
    return cardsInDeck[cardOnTopIndex];
}

void Deck::shuffle() {
    // A time-based seed for randomness
    std::srand(static_cast<unsigned int>(std::time(0)));

    // Create a random engine and shuffle the deck using Fisher-Yates algorithm
    std::random_device rd;
    std::default_random_engine rng(rd());
    std::shuffle(cardsInDeck.begin(), cardsInDeck.end(), rng);
}

void Deck::drawCard(Hand &hand) {
    hand.addCard(getCardOnTop());
    ++cardOnTopIndex;
}
