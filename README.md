# Hra Blackjack

## Zadání
Implementujte hru Blackjack pro 1 - 4 hráče.
Hráči se u hry budou střídat.
Budou postupně zadávat hit nebo stand.
Program automaticky vyhodnotí výsledek hry a tyto imformace zobrazí na terminál.

## Kompilace programu
Příkaz pro kompilaci:
```bash
cmake -Bcmake-build-debug -H. -DCMAKE_BUILD_TYPE=Debug
cmake --build cmake-build-debug
```

## Spuštění a přepínače
Hra se spouští z příkazové řádky.
Parametry programu:
- -p (--play) - počet hráčů
- -h (--help) - nápověda

Příklady spuštění programu:
```bash
Blackjack
Blackjack -p 2
Blackjack --players 3
Blackjack --help
Blackjack -h
```

## Ovládání programu
Poslední řádek výpisu je hráč, který je právě na řadě.

- s - hráč zůstane stát (stand)
- h - hráč si veme další kartu (hit)

### Průběh programu
Hráči hrají dokud nezůstanou stát. Pokud získají 21 nebo více, zůstanou stát automaticky. Hráč, který získal 21 při
rozdávání získal blackjack a dále nehraje.

Pokud všichni hráči stojí, začíná hrát krupiér. Krupiérova hra je přeskočena, pokud všichni hráči maji více než 21
nebo dostali blackjack.

### Ukončení programu
Program se ukončí automaticky poté, co všichni hráči stojí a dohraje krupiér.

Program je možné kdykoliv ukončit klávesou `Q`.

## Testování programu
Vzhledem k náhodnosti jsou některé testy validní pouze v kontextu.

### Přiklady testů pro 1 hráče:
- hráč dostal blackjack - hra se automaticky vypne, aniž by hrál krupiér
- `s` - hráč zůstane stát a dojde ke hře krupiéra a vyhodnocení
- `h` - hráč dostane unikátní kartu z balíčku
- neustálé `h` - hráč přesáhl přes 21 a hra se automaticky vypne, aniž by hrál krupiér (pouze pokud hráč nedostal 21)
- hráč má 10 nebo více a dostal eso - eso se počítá jako 1

### Přiklady testů pro více hráčů:
- hráč dostal blackjack - v dalších kolech se hráč přeskakuje
- `s` - hráč zůstane stát a v dalších kolech se přeskakuje
- hráč má 21 nebo více - v dalších kolech se přeskakuje

### Zbylé testy:
- hráč který dostal blackjack má status blackjack
- hráč který přesáhl 21 na konci hry prohrál
- hráč který má na konci hry méně než krupiér pokud krupiér nemá více než 21 prohrál
- hráč který má na konci hry více než krupiér a zároveň méně než 21 vyhrál
- hráč který má na konci hry méně než 21 a krupiér více než 21 vyhrál
- hráč který má stejně jako krupiér dostal remízu