#include "Game.h"

Game::Game(int numberOfPlayers) : dealer(new Player(5)), ui(new UI(std::cout, this)),
                                    quit(false), playersStanding(0) {
    deck.shuffle();
    for (int i = 1; i <= numberOfPlayers; ++i) {
        players.emplace_back(new Player(static_cast<size_t>(i)));
    }
    dealInitialCards();
}

void Game::dealInitialCards() {
    for (int i = 0; i < 2; ++i) {
        for (auto& player : players)
            deck.drawCard(player->getHand());
    }
    deck.drawCard(dealer->getHand());
    findBlackjack();
    ui->gameChanged();
}

void Game::drawCard(Player* player) {
    deck.drawCard(player->getHand());
    cv.notify_all();
}

void Game::playerTurn() {
    std::this_thread::sleep_for(std::chrono::milliseconds (80));
    auto activePlayer = players[activePlayerIndex];
    if (!activePlayer->isStanding() && activePlayer->handValue() < 21) {
        if (playerMove == 'h') {
            playerMove = 0;
            drawCard(activePlayer);
            ++activePlayer;
            changePlayer();
            ui->gameChanged();
        } else if (playerMove == 's') {
            playerMove = 0;
            activePlayer->stand();
            ++playersStanding;
            ++activePlayer;
            changePlayer();
            ui->gameChanged();
        }
    } else if (!activePlayer->isStanding()) {
        ++playersStanding;
        activePlayer->stand();
        changePlayer();
        ui->gameChanged();
    } else
        changePlayer();
}

void Game::dealerTurn() {
    if (!skipDealerTurn()) {
        while (dealer->handValue() < 17) {
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            drawCard(dealer);
            ui->gameChanged();
            std::this_thread::sleep_for(std::chrono::milliseconds(800));
        }
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(150));
    ui->gameEnded();
    cv.notify_all();
}

bool Game::skipDealerTurn() {
    for (auto player : players) {
        if (player->handValue() <= 21 && !player->getBlackjack())
            return false;
    }
    return true;
}

void Game::play() {
    if (players.size() == playersStanding) {
        activePlayerIndex = 5;
        dealerTurn();
        setQuit(true);
        cv.notify_one();
    } else {
        playerTurn();
        cv.notify_one();
    }
    std::this_thread::sleep_for(std::chrono::milliseconds (80));
}

void Game::changePlayer() {
    if (activePlayerIndex == players.size() - 1)
        activePlayerIndex = 0;
    else
        ++activePlayerIndex;
    if ((players[activePlayerIndex]->getBlackjack() || players[activePlayerIndex]->isStanding())
        && players.size() != playersStanding)
            changePlayer();
}

void Game::input() {
    std::unique_lock<std::mutex> lg(mtx);
    lg.unlock();
    if (quit || players.size() == playersStanding || activePlayerIndex == 5 || skipDealerTurn())
        return;
    if (std::cin.good()) {
        char c;
        std::cin >> c;
        c = static_cast<char>(std::tolower(c));
        if (c == 'q') {
            setQuit(true);
            cv.notify_one();
        } else if (c == 'h') {
            playerMove = 'h';
            cv.notify_one();
        } else if (c == 's') {
            playerMove = 's';
            cv.notify_one();
        } else {
            playerMove = 0;
        }
    } else {
        setQuit(true);
        cv.notify_one();
    }
    std::this_thread::sleep_for(std::chrono::milliseconds (500));
}

bool Game::getQuit() const {
    return quit;
}

Player * Game::getDealer() {
    return dealer;
}

void Game::output() {
    if (quit)
        return;
    std::unique_lock<std::mutex> lg(mtx);
    cv.wait(lg);
    ui->draw();
}

const std::vector<Player*> &Game::getPlayers() {
    return players;
}

Player *Game::getActivePlayer() const {
    if (activePlayerIndex == 5)
        return dealer;
    else if (!players[activePlayerIndex]->isStanding())
        return players[activePlayerIndex];
    return dealer;
}

void Game::setQuit(const bool q){
    quit = q;
}

Game::~Game() {
    for (auto& player : players) {
        delete player;
        player = nullptr;
    }
    delete dealer;
    dealer = nullptr;
    delete ui;
}

void Game::findBlackjack() {
    for (auto player : players) {
        if (player->handValue() == 21)
            player->setBlackjack();
    }
}