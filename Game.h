#ifndef BLACKJACK_GAME_H
#define BLACKJACK_GAME_H
#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include "Player.h"
#include "Deck.h"
#include "UI.h"
class UI;
class Game {
public:
    /**
     * Prepares the game - shuffles deck and creates players depending on the numberOfPlayers
     * @param numberOfPlayers given from the argument of the program
     */
    explicit Game(int numberOfPlayers);
    /**
     * Destructor
     */
    ~Game();

    /**
     * Dealing all players 2 cards and 1 card to the dealer
     */
    void dealInitialCards();
    /**
     * Deals a card to the active player
     * @param player active player
     */
    void drawCard(Player* player);
    /**
     * Handles player's input or status (blackjack/stand)
     * After the player has played moves to another player
     */
    void playerTurn();
    /**
     * Simulates dealers drawing - slow drawing
     * Dealer draws until the hand value is higher than 17
     */
    void dealerTurn();
    /**
     * Lets play players or dealer when all players are standing
     */
    void play();
    /**
     * Players getter
     * @return players
     */
    const std::vector<Player*>& getPlayers();
    /**
     * Dealer getter
     * @return dealer
     */
    Player* getDealer();
    /**
     * Handles input
     * q - quit
     * h - hit
     * s - stand
     */
    void input();
    /**
     * Quit getter
     * @return quit
     */
    bool getQuit() const;
    /**
     * Calls ui for drawing
     */
    void output();
    /**
     * Quit setter
     * @param q quit
     */
    void setQuit(bool q);
    /**
     * Active player getter
     * @return activePlayer
     */
    Player* getActivePlayer() const;
    /**
     * Changes the active player to player who is not standing
     */
    void changePlayer();
    /**
     * Finds whether a player has gotten a blackjack
     */
    void findBlackjack();
    /**
     * Skips dealer's turn when all players have above 21 or blackjack
     * @return
     */
    bool skipDealerTurn();

private:
    Deck deck;                      // game deck
    Player* dealer;                 // dealer of the game
    std::vector<Player*> players;   // vector of players
    UI* ui;                         // drawing class
    bool quit;
    std::mutex mtx;
    std::condition_variable cv;
    size_t playersStanding = 0;     // count of standing players
    size_t activePlayerIndex = 0;   // index of the active player
    char playerMove = 0;            // last input
};

#endif //BLACKJACK_GAME_H
