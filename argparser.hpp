#ifndef TIC_TAC_TOE_ARGPARSER_H
#define TIC_TAC_TOE_ARGPARSER_H

#include <iostream>
#include <string>
#include <cstring>

#define MIN_PLAYERS 1
#define MAX_PLAYERS 4
#define HELP_REQUESTED (-1)


/**
 * Terminal raw mode setting
 */
void set_raw(bool set);

class ArgParser {
public:
    /**
     * Parsing program arguments:
     *  -p or --players (number of players)
     *  -h or --help (prints help)
     */
    static int parseProgramArguments(int numberOfPlayers, char* argv[]);

    /**
     * Prints usage of the arguments
     * @param programName
     */
    static void printUsage(const char* programName);
};  

#endif //TIC_TAC_TOE_ARGPARSER_H
