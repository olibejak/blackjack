#include <iostream>
#include <vector>

#include "Card.h"

#ifndef BLACKJACK_HAND_H
#define BLACKJACK_HAND_H

class Hand {
public:

    /**
     * Constructor
     */
    Hand() = default;

    /**
     * Calculates the hand value
     * Firs calculate the total value of non-Ace cards,
     * then adds Ace values to the total, counting them as 11 until it would exceed 21
     * @return value of the hand
     */
    size_t getValue() const;

    /**
     * Adds card to the hand
     * @param card given
     */
    void addCard(const Card &card);

    /**
     * @return toString of every card in the hand
     */
    std::string toString() const;

private:
    std::vector<Card> cards;    // cards in the hand
};


#endif //BLACKJACK_HAND_H
