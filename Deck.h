#ifndef BLACKJACK_DECK_H
#define BLACKJACK_DECK_H


#include <vector>
#include "Card.h"
#include "Player.h"

class Deck {
public:
    /**
     * Constructor
     * Creates cards
     */
    Deck();

    /**
     * Cards in the deck getter
     * @return cards in the deck
     */
    std::vector<Card> getCards() const;

    /**
     * Card on top of the deck getter
     * @return Card on top of the deck
     */
    Card getCardOnTop();

    /**
     * Shuffles cards in the deck
     * Uses a time-based seed for randomness
     * Creates a random engine and shuffle the deck using Fisher-Yates algorithm
     */
    void shuffle();

    /**
     * Inserts a card from top of the deck into players hand
     * Moves cardOnTop index
     * @param hand hand of the player that is drawing the card
     */
    void drawCard(Hand &hand);


private:
    std::vector<Card> cardsInDeck;   // every card that hasn't been drawn
    size_t cardOnTopIndex;           // index of a card on top of the deck
};


#endif //BLACKJACK_DECK_H
