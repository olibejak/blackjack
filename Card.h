#ifndef BLACKJACK_CARD_H
#define BLACKJACK_CARD_H

#include <string>
#include <stdexcept>

enum Suit { HEARTS, DIAMONDS, CLUBS, SPADES };
enum Rank { ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING };

class Card {
public:

    /**
     * Constructor
     * @param suit { HEARTS, DIAMONDS, CLUBS, SPADES }
     * @param rank { ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING }
     */
    Card(Suit suit, Rank rank);

    /**
     * Suit getter
     * @return suit
     */
    Suit getSuit() const;

    /**
     * Rank getter
     * @return rank
     */
    Rank getRank() const;

    /**
     * @return rank + suit
     */
    std::string toString() const;

    /**
     * Converts rank into string for toString method.
     * @return numeric value for numeric ranks and string value for ace, jack, queen, king
     */
    std::string getStringValueOfRank() const;

    /**
     * Converts rank into string for toString method.
     * @return unicode for each suit
     */
    std::string getStringValueOfSuit() const;

private:
    Suit suit;  // { HEARTS, DIAMONDS, CLUBS, SPADES }
    Rank rank;  // { ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING }
};


#endif //BLACKJACK_CARD_H
