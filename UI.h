#ifndef BLACKJACK_UI_H
#define BLACKJACK_UI_H
#include <iostream>
#include <string>
#include "Game.h"
#include <sstream>

// ANSI escape codes
#define CLEAR   "\x1B[2J\x1B[H"
#define RED     "\033[31m"
#define BLUE    "\033[34m"
#define GREEN   "\033[32m"
#define YELLOW  "\033[33m"
#define CYAN    "\033[36m"
#define RESET   "\033[0m"

class Game;

/**
 * Handles output of the game
 */
class UI {
public:

    /**
     * Constructor
     * @param outputStream
     * @param gamePtr game reference
     */
    UI(std::ostream &outputStream, Game* gamePtr);

    /**
     * Main method, updates output stream and decides what is being inserted into the output stream
     * Calls redraw when game has changed
     * Calls drawEnd when game has ended
     */
    void draw();

    /**
     * Inserts game status and currently playing player into the string stream
     */
    void redraw();

    /**
     * Inserts game ending into the string stream
     */
    void drawEnd();

    /**
     * Decides player's status after game ends
     * @param player
     * @return status in a stream
     */
    std::string gameOutcome(Player* player);

    /**
     * Chooses color of a player
     * @param colorId id of a player
     * @param buffer string stream to insert into
     */
    static void setConsoleColor(size_t colorId, std::stringstream &buffer);

    /**
     * Called when the game has ended
     */
    void gameEnded();

    /**
     * Called when the game has changed
     */
    void gameChanged();

private:
    Game *game;                     // game reference
    std::ostream &outputStream;     // reference of the output (std::cout)
    bool changed = false;           // boolean whether the game has changed
    bool end = false;               // boolean whether the game has ended
    std::stringstream ss;           // stream of the game
};

#endif //BLACKJACK_UI_H