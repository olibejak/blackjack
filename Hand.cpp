#include <sstream>
#include "Hand.h"

size_t Hand::getValue() const {
    size_t handValue = 0;
    size_t acesInHand = 0;

    // Calculate the total value of non-Ace cards
    for (auto card : cards) {
        if (static_cast<size_t>(card.getRank()) != 0 && static_cast<size_t>(card.getRank()) <= 9)
            handValue += static_cast<size_t>(card.getRank() + 1);
        else if (static_cast<size_t>(card.getRank()) != 0 && static_cast<size_t>(card.getRank()) > 9)
            handValue += 10;
        else
            ++acesInHand;
    }

    // Add Ace values to the total, counting them as 11 until it would exceed 21
    for (size_t i = 0; i < acesInHand; ++i) {
        if (handValue + 11 <= 21) {
            handValue += 11;
        } else if (handValue + 11 > 21) {
            handValue += 1;
        }
    }

    return handValue;
}

void Hand::addCard(const Card &card) {
    cards.emplace_back(card);
}

std::string Hand::toString() const {
    std::stringstream result;
    for (size_t cardIndex = 0; cardIndex < cards.size() - 1; ++cardIndex)
        result << cards[cardIndex].toString() + ", ";
    result << cards[cards.size() - 1].toString() << "\n";
    return result.str();
}