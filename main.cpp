#include <thread>
#include "Hand.h"
#include "Game.h"
#include "argparser.hpp"

int main(int argc, char *argv[]) {
    auto param = ArgParser::parseProgramArguments(argc, argv);
    if (param == HELP_REQUESTED) {
        return EXIT_SUCCESS;
    }

    auto computeThread = [](Game& game) {
        bool q = false;
        while (!q) {
            game.play();
            q = game.getQuit();
        }
    };
    auto outputThread = [](Game& game) {
        bool q = false;
        while (!q) {
            game.output();
            q = game.getQuit();
        }
    };
    auto inputThread = [](Game& game) {
        bool q = false;
        while (!q) {
            game.input();
            q = game.getQuit();
        }
    };
    set_raw(true);
    Game game(param);
    std::thread t1(computeThread, std::ref(game));
    std::thread t2(outputThread, std::ref(game));
    std::thread t3(inputThread, std::ref(game));

    t1.join();
    t2.join();
    t3.join();
    set_raw(false);
    return 0;
}