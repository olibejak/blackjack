#ifndef BLACKJACK_PLAYER_H
#define BLACKJACK_PLAYER_H


#include <string>
#include "Hand.h"
#include <utility>


class Player {
public:
    /**
     * Constructor
     * @param id
     */
    explicit Player(size_t id);

    /**
     * Hand getter
     * @return player's hand
     */
    Hand & getHand();

    /**
     * Sets standing to true
     */
    void stand();

    /**
     * Id getter
     * @return player's id
     */
    size_t getId() const;

    /**
     * Standing getter
     * @return standing
     */
    bool isStanding() const;

    /**
     * Calls value getter from the hand class
     * @return value of player's hand
     */
    size_t handValue() const;

    /**
     * Blackjack getter
     * @return blackjack
     */
    bool getBlackjack() const;

    /**
     * Sets blackjack to true
     */
    void setBlackjack();

private:
    size_t id;                  // player's id
    Hand hand;                  // hand of the player
    bool standing = false;      // whether is the player standing
    bool blackjack = false;     // whether the player has gotten a blackjack (21 from 2 cards)
};


#endif //BLACKJACK_PLAYER_H
