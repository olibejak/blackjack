#include "Player.h"

Player::Player(size_t id) : id(id) {}

size_t Player::getId() const{
    return id;
}

Hand & Player::getHand() {
    return hand;
}

bool Player::isStanding() const{
    return standing;
}

bool Player::getBlackjack() const{
    return blackjack;
}

void Player::setBlackjack() {
    blackjack = true;
}

void Player::stand() {
     standing = true;
}

size_t Player::handValue() const{
    return hand.getValue();
}
