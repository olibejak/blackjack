#include "argparser.hpp"

int ArgParser::parseProgramArguments(int argc, char* argv[]) {
    if (argc < 2) {
        // If no arguments provided, return MIN_PLAYERS
        return MIN_PLAYERS;
    }

    if (std::strcmp(argv[1], "--help") == 0 || std::strcmp(argv[1], "-h") == 0) {
        // If --help option is provided, print usage information
        printUsage(argv[0]);
        return HELP_REQUESTED;
    }

    if (argc < 3 || (std::strcmp(argv[1], "-p") != 0 && std::strcmp(argv[1], "--players") != 0)) {
        // If incorrect arguments provided, return HELP_REQUESTED
        std::cerr << "Error: Incorrect arguments.\n";
        printUsage(argv[0]);
        return HELP_REQUESTED;
    }

    if (std::strcmp(argv[1], "--players") == 0 || std::strcmp(argv[1], "-p") == 0) {
        // Attempt to convert the argument to an integer
        int numPlayers = std::atoi(argv[2]);

        if (numPlayers <= MIN_PLAYERS)
            return MIN_PLAYERS;
        else if (numPlayers >= MAX_PLAYERS)
            return MAX_PLAYERS;
        else
            return numPlayers;
    } else

    return HELP_REQUESTED;
}

void ArgParser::printUsage(const char* programName) {
    std::cerr << "Usage: " << programName << " -p [number]" << std::endl;
    std::cerr << "Options:\n";
    std::cerr << "  -p [number]   Specify the number of players (between "
              << MIN_PLAYERS << " and " << MAX_PLAYERS << ")\n";
    std::cerr << "  --help        Display this help message\n";
}

void set_raw(bool set) {
    if (set) {
        system("stty raw");  // enable raw
    } else {
        system("stty -raw"); // disable raw
    }
}