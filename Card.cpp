#include "Card.h"

Card::Card(Suit suit, Rank rank) : suit(suit), rank(rank) {}

Suit Card::getSuit() const {
    return suit;
}

Rank Card::getRank() const {
    return rank;
}

std::string Card::getStringValueOfRank() const {
    switch (rank) {
        case ACE:
            return "Ace";
        case JACK:
            return "Jack";
        case QUEEN:
            return "Queen";
        case KING:
            return "King";
        default:
            return std::to_string(static_cast<size_t>(rank) + 1);
    }
}

std::string Card::getStringValueOfSuit() const {
    switch (suit) {
        case HEARTS:
            return u8"\u2665"; // Unicode for heart symbol
        case DIAMONDS:
            return u8"\u2666"; // Unicode for diamond symbol
        case CLUBS:
            return u8"\u2663"; // Unicode for club symbol
        case SPADES:
            return u8"\u2660"; // Unicode for spade symbol
        default:
            throw std::runtime_error("Invalid suit value for Card");;
    }
}

std::string Card::toString() const {
    return getStringValueOfRank() + getStringValueOfSuit();
}

